### KVM 1：Debian Host

服务器上只在Debian Host上运行Docker，虽然images可以建很多，但只适合跑Docker，单纯Linux系统有时也需要，考虑多种应用场景，还应该用VM。

之前用Debian+VirtualBox+Vagrant+Docker有两个问题，一是VirtualBox现在属于Oracle，另外，遇到Dockerfile在VM里报错。

现在设想建KVM VM+Docker，Debian 8 Host，KVM + QEMU VM，两种VMs，VM_L里面运行Docker，VM_S做服务器VM。


下载64位Debian8.2，从大连理工服务器下载。路径：

http://mirror.dlut.edu.cn/debian-cdimage/current/amd64/iso-cd/debian-8.2.0-amd64-CD-1.iso

// Rufus制作Bootable USB Flash

Rufus打开，从镜像选项选ISO，打开图标，找到iso文件，开始复制，从制作的这个USB启动。注意设置BIOS启动顺序。

// 安装Debian